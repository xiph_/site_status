#/bin/bash

# ARGs
SITE="${1}"

# Vars
image_tag='py:sitestatus'

src_dir="docker"
repo='https://gitlab.com/xiph_/site_status'

# Incase manual sync is ever needed as a part of the script
# But might just be over engineering
function git_clone {
    if [[ ! -d "${work_dir}" && ! -z "${work_dir}" ]]
    then
        echo "No such directory: ${work_dir}"
        return 1
    fi
    
    git clone "${repo}" "${work_dir}"
    rc=$?

    return 0
}

function build_image {
    docker build --no-cache "${src_dir}" -t "py:sitestatus"
    rc=$?

    return 0
}

function run_docker_compose {
    export site_env="${SITE}"
    docker-compose up
    rc=$?

    return 0
}

function main {
    if [[ -z "${SITE}" ]]
    then
        echo "ERROR: Must specify a website URL as argument 1"
        return 1
    fi
    
    build_image
    rc=$?

    if [[ $rc -ne 0 ]];
    then
        echo "Error: Failed to build docker image, aborting"
        return 1
    fi

    run_docker_compose
    if [[ $rc -ne 0 ]];
    then
        echo "Error: Failed to run docker-compose"
        return 1
    fi
}

main $@

rc=$?
exit $rc
