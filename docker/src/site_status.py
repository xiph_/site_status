#!/usr/bin/env python3

# Generic script to check if a website is UP
# Check is done via DNS resolution & HTTP status
# If we had a specific site in mind,
# we could check if a specific HTML element loads with Selenium

from sys import argv

import requests
import dns.resolver

def check_dns(url, verbose=False):
    try:
        ans = dns.resolver.resolve(url, 'A')
    except dns.resolver.NoNameservers:
        if(verbose):
            print("ERROR: Failed to resolve domain: {}".format(url))
        return False
    except dns.resolver.NXDOMAIN:
        if(verbose):
            print("ERROR: The DNS query name does not exist")
        return False

    ip = [ addr for addr in ans ]
    return ip

def http_status(url, verbose=False):
    try:
        return requests.get(url).status_code
    except requests.exceptions.ConnectionError:
        if (verbose):
            print("ERROR: Failed to connect to URL: {}".format(url))
        return False

def usage(script_name):
    print("{} <url>".format(script_name))


if __name__ == '__main__':
    if (len(argv) < 2):
        usage(argv[0])
        exit(1)
    else:
        if( '//' in argv[1]):
            url = argv[1].split('//')
            domain = url[1]
            http_type = url[0]
        else:
            usage(argv[0])
            print("Error: URL must start with 'HTTP://' or 'HTTPS://'")
            exit(1)
                 
    if (check_dns(domain) and http_status("{}//{}".format(http_type, domain)) == 200):
        print("OK")
        exit(0)
    else:
        print("Failure")
        exit(1)

